<?php

namespace FullCycle\API;

class APIConfigClass {
	static protected $apiBase = false;
	static protected $apiAccessKey = false;

	static public function setApiAccessKey($key) {
		static::$apiAccessKey=$key;	
	}

        static public function getApiBase() {
            return static::$apiBase;
        }

        static public function getApiAccessKey() {
            return static::$apiAccessKey;
        }


}



