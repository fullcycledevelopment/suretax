<?php 

namespace FullCycle\SureTax;

use FullCycle\SureTax\SureTaxAPIResource;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class SureTaxFinalizePostRequest extends SureTaxAPIResource {
    protected $_request_url="FinalizePostRequest";
    protected $_method = "POST";
        
    function __construct($id = null, $opts = null) {
        $this->initHeader();
        $data = $this->_header;
        $data['MasterTransId'] = $id;
        /*
        if ($id)
            $data['ItemList'] = $id;
        else 
            $data['ItemList']=[];
        */
        parent::__construct($data,$opts);
        $this->optionAdjust();
    }
 
    /*
    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}{$this->getRequestUrl()}";
        return $uri;
    }
    */
    
    function setTotal() {
        $sum=0;
        foreach($this->_retrieveOptions['ItemList'] as $item) {
            $sum += $item["Revenue"];
        }
        $this->_retrieveOptions["TotalRevenue"] = $sum;
    }
    
    function getRetrieveOptions() {
        $json_data['requestFinalize'] = json_encode($this->_retrieveOptions);
        return $json_data;
    }
    
    function refresh() {
        parent::refresh();
    }
    
    function initHeader(array $options = []) {
        //parent::initHeader();    
        $this->_header = [];
        $this->_header["ClientNumber"] = SureTaxAPIConfig::getApiClientId();
        $this->_header["ValidationKey"] = SureTaxAPIConfig::getApiAccessKey();
        $this->_header["ClientTracking"] = "1234";
    }    
    
}
