<?php

namespace FullCycle\API;

class APIObject implements \ArrayAccess , \Countable { //, \JsonSerializable {

    protected $_opts;
    protected $_originalValues;
    protected $_values;
    protected $_unsavedValues;
    protected $_transientValues;
    protected $_retrieveOptions;
    protected $_lastResponse;
    
    public function __construct($id = null, $opts = null)
    {
//        list($id, $this->_retrieveOptions) = Util\Util::normalizeId($id);
        $this->_retrieveOptions = $id;
//        $this->_opts = Util\RequestOptions::parse($opts);
        $this->_opts = $opts;
        $this->_originalValues = [];
        $this->_values = [];
        $this->_unsavedValues = new Util\Set();
        $this->_transientValues = new Util\Set();
        if ($id !== null) {
            $this->_values['id'] = $id;
        }
    }
    

    // Standard accessor magic methods
    public function __set($k, $v)
    {
        /*
        if (static::getPermanentAttributes()->includes($k)) {
            throw new \InvalidArgumentException(
                "Cannot set $k on this object. HINT: you can't set: " .
                join(', ', static::getPermanentAttributes()->toArray())
            );
        }
        */
        if ($v === "") {
            throw new \InvalidArgumentException(
                'You cannot set \''.$k.'\'to an empty string. '
                .'We interpret empty strings as NULL in requests. '
                .'You may set obj->'.$k.' = NULL to delete the property'
            );
        }
        $this->_values[$k] = Util\Util::convertToAPIObject($v, $this->_opts);
        $this->dirtyValue($this->_values[$k]);
        $this->_unsavedValues->add($k);
    }
    public function __isset($k)
    {
        return isset($this->_values[$k]);
    }
    public function __unset($k)
    {
        unset($this->_values[$k]);
        $this->_transientValues->add($k);
        $this->_unsavedValues->discard($k);
    }
    public function &__get($k)
    {
        // function should return a reference, using $nullval to return a reference to null
        $nullval = null;
        if (!empty($this->_values) && array_key_exists($k, $this->_values)) {
            return $this->_values[$k];
        }
        return $nulval;
        
        /*
        } else if (!empty($this->_transientValues) && $this->_transientValues->includes($k)) {
            $class = get_class($this);
            $attrs = join(', ', array_keys($this->_values));
            $message = "API Notice: Undefined property of $class instance: $k. "
                    . "HINT: The $k attribute was set in the past, however. "
                    . "It was then wiped when refreshing the object "
                    . "with the result returned by API's API, "
                    . "probably as a result of a save(). The attributes currently "
                    . "available on this object are: $attrs";
            APIConfigClass::getLogger()->error($message);
            return $nullval;
        } else {
            $class = get_class($this);
            APIConfigClass::getLogger()->error("API Notice: Undefined property of $class instance: $k");
            return $nullval;
        }
        */
    }


    // ArrayAccess methods
    public function offsetSet($k, $v)
    {
        $this->$k = $v;
    }
    public function offsetExists($k)
    {
        return array_key_exists($k, $this->_values);
    }
    public function offsetUnset($k)
    {
        unset($this->$k);
    }
    public function offsetGet($k)
    {
        return array_key_exists($k, $this->_values) ? $this->_values[$k] : null;
    }
    
    // Countable method
    public function count()
    {
        return count($this->_values);
    }
    public function keys()
    {
        return array_keys($this->_values);
    }
    public function values()
    {
        return array_values($this->_values);
    }


    public static function constructFrom($values, $opts = null)
    {
        $obj = new static(isset($values['id']) ? $values['id'] : null);
        $obj->refreshFrom($values, $opts);
        return $obj;
    }

    /**
     * Refreshes this object using the provided values.
     *
     * @param array $values
     * @param null|string|array|Util\RequestOptions $opts
     * @param boolean $partial Defaults to false.
     */
    public function refreshFrom($values, $opts = null, $partial = false)
    {
//        $this->_opts = Util\RequestOptions::parse($opts);
        $this->_originalValues = self::deepCopy($values);
        if ($values instanceof APIObject) {
            $values = $values->__toArray(true);
        }
        // Wipe old state before setting new.  This is useful for e.g. updating a
        // customer, where there is no persistent card parameter.  Mark those values
        // which don't persist as transient
        
        if ($partial) {
            $removed = new Util\Set();
        } else {
            $removed = new Util\Set(array_diff(array_keys($this->_values), array_keys($values)));
        }
        foreach ($removed->toArray() as $k) {
            unset($this->$k);
        }
        $this->updateAttributes($values, $opts, false);
        foreach ($values as $k => $v) {
            $this->_transientValues->discard($k);
            $this->_unsavedValues->discard($k);
        }
    }

    /**
     * Mass assigns attributes on the model.
     *
     * @param array $values
     * @param null|string|array|Util\RequestOptions $opts
     * @param boolean $dirty Defaults to true.
     */
    public function updateAttributes($values, $opts = null, $dirty = true)
    {
        foreach ($values as $k => $v) {
            // Special-case metadata to always be cast as a APIObject
            // This is necessary in case metadata is empty, as PHP arrays do
            // not differentiate between lists and hashes, and we consider
            // empty arrays to be lists.
            if ($k === "metadata") {
                $this->_values[$k] = APIObject::constructFrom($v, $opts);
            } else {
                $this->_values[$k] = Util\Util::convertToAPIObject($v, $opts);
            }
            if ($dirty) {
                $this->dirtyValue($this->_values[$k]);
            }
            $this->_unsavedValues->add($k);
        }
    }
    
    /**
     * Sets all keys within the APIObject as unsaved so that they will be
     * included with an update when `serializeParameters` is called. This
     * method is also recursive, so any APIObjects contained as values or
     * which are values in a tenant array are also marked as dirty.
     */
    public function dirty()
    {
        $this->_unsavedValues = new Util\Set(array_keys($this->_values));
        foreach ($this->_values as $k => $v) {
            $this->dirtyValue($v);
        }
    }
    protected function dirtyValue($value)
    {
        if (is_array($value)) {
            foreach ($value as $v) {
                $this->dirtyValue($v);
            }
        } elseif ($value instanceof APIObject) {
            $value->dirty();
        }
    }
    
    
    /**
     * Produces a deep copy of the given object including support for arrays
     * and APIObject.
     */
    protected static function deepCopy($obj)
    {
        if (is_array($obj)) {
            $copy = [];
            foreach ($obj as $k => $v) {
                $copy[$k] = self::deepCopy($v);
            }
            return $copy;
        } elseif ($obj instanceof APIObject) {
            return $obj::constructFrom(
                self::deepCopy($obj->_values),
                clone $obj->_opts
            );
        } else {
            return $obj;
        }
    }

    public function __toArray($recursive = false)
    {
        if ($recursive) {
            return Util\Util::convertAPIObjectToArray($this->_values);
        } else {
            return $this->_values;
        }
    }
    
    
    public function toArray() {
        return $this->__toArray(true);
    }
    
}
