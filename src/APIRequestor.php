<?php 

namespace FullCycle\API;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class APIRequestor {
    
    private $_apiKey;
    private $_apiBase;
    
    
    function __construct($_apiKey = null, $_apiBase = null) {
        $this->_apiKey = $_apiKey;
        $this->_apiBase = $_apiBase;
        
    }
    
    function request($method, $url, $params = null, $headers = null) {
        $params = $params ?: [];
        $headers = $headers ?: [];
        
        return $this->_requestRaw($method,$url,$params,$headers);
    }
    
    private function _requestRaw($method, $url, $params, $headers)
    {
        
        $this->client = new Client();
        $method = strtoupper($method);
        //$json_data['request'] = json_encode($params);
       // $opts['json'] =$json_data;
        $opts['json'] =$params;
        //$opts['debug'] = true;
        
        try {
            $resp = $this->client->request($method,$url,$opts);
        } catch (\Exception $e) {
                echo "Exception: {$e->getMessage()}\n";
                //echo $e->getResponse()->getBody();
                throw $e;
        }
        return $resp;
    }
    
    private function _defaultHeaders($apiKey) {
        $defaultHeaders = [
 //           'Token' => $apiKey,
            'Authorization' => "OAuth {$apiKey}",
        ];
        return $defaultHeaders;
        
    }
    
}

