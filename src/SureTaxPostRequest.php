<?php 

namespace FullCycle\SureTax;

use FullCycle\SureTax\SureTaxAPIResource;

/**
 * @author thrasher
 * 
 * @example
 * 
 */

class SureTaxPostRequest extends SureTaxAPIResource {
    protected $_request_url="PostRequest";
    protected $_method = "POST";
        
    function __construct($id = null, $opts = null) {
        $this->initHeader();
        $data = $this->_header;
        if ($id)
            $data['ItemList'] = $id;
        else 
            $data['ItemList']=[];
        parent::__construct($data,$opts);
        $this->optionAdjust();
    }
 
    /*
    function makeUri() {
        $suretax_url = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx/PostRequest";
        return $suretax_url;
    }
    */
    
    function setTotal() {
        $sum=0;
        foreach($this->_retrieveOptions['ItemList'] as $item) {
            $sum += $item["Revenue"];
        }
        $this->_retrieveOptions["TotalRevenue"] = $sum;
    }
    
    function getRetrieveOptions() {
        $json_data['request'] = json_encode($this->_retrieveOptions);
        return $json_data;
    }
    
    function refresh() {
        $this->setTotal();
        parent::refresh();
    }
    
    function initHeader(array $options = []) {
        parent::initHeader();    
    }
    
    function addItem($item) {
        $this->_retrieveOptions['ItemList'][]=$item;
    }
}
