<?php 

namespace FullCycle\API;

//use FullCycle\API\APIObject;

class APIResource extends \FullCycle\API\APIObject {
    
    protected $_request_url=null;
    protected $_method = "GET";
    protected $_identifier = null;
    
    function __construct($id=null, $opts = null) {
        parent::__construct($id,$opts);
        
    }
    
    function getApiVersion() {
        return $this->_apiConfigClass::getApiVersion();
    }
    
    function getApiBaseUrl() {
        return $this->_apiConfigClass::getApiBase();
    }
    
    function getRequestUrl() {
        return $this->_request_url;
    }
    
    function setIdentifer($identifer) {
        $this->_identifier = $identifer;
    }
    
    function getIdentifier() {
        if (!$this->_identifier) {
            throw new \Exception("Must have an identifer set for URL  of form https://base/version/identifer/action.");
        }
        return $this->_identifier;
    }
    
    static public function create($id=null, $opts=null) {
        
        $instance = new static($id, $opts);
        $instance->refresh();
        return $instance;
    }
    
    function refresh() {
        $uri = $this->makeUri();
        $requestor= new \FullCycle\API\APIRequestor();
        $resp = $requestor->request($this->_method,$uri,$this->getRetrieveOptions());
        $this->refreshFrom($this->json_decode($resp->getBody(),true));            
    }
    
    function json_decode($json) {
        return json_decode($json,true);
    }
    
    function getRetrieveOptions() {
        return $this->_retrieveOptions;
    }
    
    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}/{$this->getApiVersion()}/{$this->getIdentifier()}/{$this->getRequestUrl()}";
        return $uri;
    }
    
    function next() {
        if ($this->paging->next) {
            $this->_retrieveOptions['after'] = $this->paging->cursors->after;
            $this->refresh();
            return $this;
        }
        return false;
    }
    
}

