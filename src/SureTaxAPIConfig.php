<?php

namespace FullCycle\SureTax;

use FullCycle\API\APIConfigClass;
use \Exception;

class SureTaxAPIConfig extends APIConfigClass {
	static protected $apiBase="https://testapi.taxrating.net/Services/General/V01/SureTax.asmx";
	static protected $apiClientId = false;
	static protected $businessUnit = false;
	static protected $apiProductionBase = "https://api.taxrating.net/Services/General/V01/SureTax.asmx";
	static protected $apiSandboxBase = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx";
	static protected $apiVersion = "V01";
	
	static function setApiClientId($id) {
	    static::$apiClientId = $id;
	}
	
	static function getApiClientId() {
	    if (static::$apiClientId === false)
	        throw new Exception("API Client Id is not set");
	    return static::$apiClientId;
	}

	static function getBusinessUnit() {
	    return static::$businessUnit;
	}
	
	static function setBusinessUnit($unit) {
	    static::$businessUnit = $unit;
	}
	
	static function setProduction() {
	    static::$apiBase = static::$apiProductionBase;
	}
	
	static function setSandbox() {
	    static::$apiBase = static::$apiSandboxBase;
	}
	
	static function getVersion() {
	    return static::$apiVersion;
	}
}

