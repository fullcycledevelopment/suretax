<?php

namespace FullCycle\SureTax;

use FullCycle\API\APIResource;
use Carbon\Carbon;
use FullCycle\SureTax\SureTaxAPIConfig;

class SureTaxAPIResource extends APIResource {
    protected $_header;
    protected $_data;
    protected $_apiConfigClass=SureTaxAPIConfig::class;
    
    function initHeader(array $options=[]) {
        // Web Request Header
        $cnow = Carbon::now();
        $this->_header["ClientNumber"] = SureTaxAPIConfig::getApiClientId();
        $this->_header["ValidationKey"] = SureTaxAPIConfig::getApiAccessKey();
        $this->_header["ClientTracking"] = "1234";
        
        $this->_header["BusinessUnit"] = SureTaxAPIConfig::getBusinessUnit();
        $this->_header["DataYear"] = $cnow->year;
        $this->_header["DataMonth"] = $cnow->month;
        
        $this->_header["ReturnFileCode"] = "0";    // 0 == Live
        $this->_header["ReturnFileCode"] = "Q";    // Q == Quote
        
        $this->_header["ResponseType"] = "D";
        
        $this->_header["TotalRevenue"] = 100.00;
        
        // My additons
        $this->_header["CmplDataYear"] = $cnow->year;
        $this->_header["CmplDataMonth"] = $cnow->month;
    }
    
    function makeUri() {
        $uri = "{$this->getApiBaseUrl()}/{$this->getRequestUrl()}";
        return $uri;
    }
    
    function mergeRetrieveOptions($options) {
        $this->_retrieveOptions=array_merge($this->_retrieveOptions,$options);
    }
    
    function optionAdjust() {
        if (!empty($this->_opts['options'])) {
            $this->mergeRetrieveOptions($this->_opts['options']);
        }
    }
    
    function json_decode($json) {
        $decode = json_decode($json,true);
        return json_decode($decode["d"],true);
    }
    
}