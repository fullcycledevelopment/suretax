<?php

//$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();


function env($var, $default = false) {
	$e = getenv($var);
	if (empty($e))
		return $default;
	return $e;
}


