<?php

use FullCycle\API\APIRequestor;
use FullCycle\SureTax\SureTaxPostRequest;
require_once 'boot.php';

echo "testing\n";

$BillingAddress = $ShipToAddress = $ShipFromAddress = $OrderPlacementAddress = $OrderApprovalAddress = [
    "PrimaryAddressLine" => "16120 Nall Ave",
    "County" => "",
    "City" => "Stilwell",
    "State" => "KS",
    "PostalCode" => "66085",
    "Country" => "US",
];

// Web Request Header
$header["ClientNumber"] = env("CCH_CLIENT_ID");
$header["BusinessUnit"] = env("CCH_BUSINESS_UNIT");
$header["ValidationKey"] = env("CCH_VALIDATION_KEY");

$header["DataYear"] = "2019";
$header["DataMonth"] = "1";
$header["TotalRevenue"] = 100.00;
$header["ReturnFileCode"] = "0";
$header["ClientTracking"] = "1234";
$header["ResponseType"] = "D";
// My additons
$header["CmplDataYear"] = "2019";
$header["CmplDataMonth"] = "1";


// End
// Input Record 1
$datarecord["LineNumber"] = "1";
$datarecord["InvoiceNumber"] = "112233";
$datarecord["CustomerNumber"] = "000000001";
$datarecord["LocationCode"] = "";
$datarecord["TransDate"] = "2019-01-17";
$datarecord["Revenue"] = 100.00;
$datarecord["Units"] = 1;
$datarecord["TaxIncludedCode"] = "0";
$datarecord["TaxSitusRule"] = "22";
$datarecord["TransTypeCode"] = "990101";
$datarecord["SalesTypeCode"] = "D";
$datarecord["RegulatoryCode"] = "01";
$datarecord["TaxExemptionCodeList"] = array( "00");
$datarecord["UDF"] = "";
$datarecord["UDF2"] = "";
$datarecord["FreightOnBoard"] = "";
$datarecord["ShipFromPOB"] = false;
$datarecord["MailOrder"] = false;
$datarecord["CommonCarrier"] = false;
$datarecord["AuxRevenue"] = "0";
//$datarecord["AuxRevenue"] = "0m";

$datarecord["CostCenter"] = "";
$datarecord["GLAccount "] = "";
$datarecord["MaterialGroup "] = "";
$datarecord["CurrencyCode"] = "";
$datarecord["ExemptReasonCode"] = "";
$datarecord["BillingAddress"] = $BillingAddress;
$datarecord["ShipToAddress"] = $ShipToAddress;
$datarecord["ShipFromAddress"] = $ShipFromAddress;
$datarecord["OrderPlacementAddress"] = $OrderPlacementAddress;
$datarecord["OrderApprovalAddress "] = $OrderApprovalAddress;

$url = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx";

$url="https://testapi.taxrating.net/Services/V07/SureTax.asmx/PostRequest";

$data = $header;
$data['ItemList'][] = $datarecord;


$json_data['request'] = $header;
$json_data['request']['ItemList'][] = $datarecord;

print_r($data);

$my_data = json_encode($data);
echo $my_data;
echo "\n------------------\n";


$json = json_encode($header); 
$json = substr($json,0,strlen($json)-1);
$json .= ',"ItemList": [';

$json .= json_encode( $datarecord );

$json .= ']}';

/*
$my_data = $json; 
echo $my_data;
echo "\n------------------\n";
*/

// SureTax web request API URL
$suretax_url = "https://testapi.taxrating.net/Services/V07/SureTax.asmx/PostRequest";
$suretax_url = "https://testapi.taxrating.net/Services/V01/SureTax.asmx/PostRequest";
$suretax_url = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx/PostRequest";

$headers = [
    'Content-Type: application/json',
    'ContentLength: ' .strlen($my_data),
];

//Initiate cURL request
$ch = curl_init();
// Set Headers
curl_setopt($ch, CURLOPT_POSTFIELDS, "request=$my_data" );
//curl_setopt($ch, CURLOPT_POSTFIELDS, $my_data );

curl_setopt($ch, CURLOPT_POST, 1 ); 
curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0 ); 
//curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
curl_setopt($ch,CURLOPT_URL, $suretax_url );
curl_setopt($ch, CURLOPT_TIMEOUT, 10 ); // 10 second timeout option
$response = curl_exec( $ch ); 
curl_close($ch );

echo "done\n";
print_r($response);
echo "\n";

echo "Request2\n";

$my_data = '{"request":' ."'";
$my_data.= json_encode($data) . "'" ;
$my_data.= '}'; 

$headers = [
    'Content-Type: application/json',
    'ContentLength: ' .strlen($my_data),
];

echo $my_data . "\n";
echo "\n------------------\n";

echo json_encode($json_data);
echo "\n------------------\n";

$my_data = json_encode(['request'=>json_encode($json_data)]);

//Initiate cURL request
$ch = curl_init();
// Set Headers
// curl_setopt($ch, CURLOPT_POSTFIELDS, "request=$my_data" );
curl_setopt($ch, CURLOPT_POSTFIELDS, $my_data );

curl_setopt($ch, CURLOPT_POST, 1 );
curl_setopt($ch,CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0 );
curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0 );
curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
curl_setopt($ch,CURLOPT_URL, $suretax_url );
curl_setopt($ch, CURLOPT_TIMEOUT, 10 ); // 10 second timeout option
$response = curl_exec( $ch );
curl_close($ch );

echo "done\n";
print_r($response);
echo "\n";


echo "\n------------------\n";


$requestor = new APIRequestor();
$header=[];
$resp = $requestor->request("post",$suretax_url,$data);
print_r($resp->getBody());
echo $resp->getBody();
//print_r( $resp->getHeader());

echo "\n------------------\n";

$x = SureTaxPostRequest::create($data);

echo "Response\n";
print_r($x->__toArray(true));

echo "\n------------------\n";
