<?php

require_once 'boot.php';

use Carbon\Carbon;

use FullCycle\API\APIRequestor;
use FullCycle\SureTax\SureTaxPostRequest;
use FullCycle\SureTax\SureTaxAPIConfig;
use FullCycle\SureTax\SureTaxFinalizePostRequest;

function setupConfig() {
    SureTaxAPIConfig::setApiAccessKey(env("CCH_VALIDATION_KEY"));
    SureTaxAPIConfig::setApiClientId(env("CCH_CLIENT_ID"));
    SureTaxAPIConfig::setBusinessUnit(env("CCH_BUSINESS_UNIT"));
}

setupConfig();

echo "testing\n";

$trump_tower = [
    "PrimaryAddressLine" => "725 5th Ave",
    "County" => "",
    "City" => "New York",
    "State" => "NY",
    "PostalCode" => "10022",
    "Country" => "US",
//    "Plus4"=>"8943",
];


$my_address = [
    "PrimaryAddressLine" => "16120 Nall Ave",
    "County" => "",
    "City" => "Stilwell",
    "State" => "KS",
    "PostalCode" => "66085",
    "Country" => "US",
 //   "Plus4"=>"8943",
];

$microsoft  = [
    "PrimaryAddressLine" => "2624 NE University Village St",
    "County" => "",
    "City" => "Seattle",
    "State" => "WA",
    "PostalCode" => "98105",
    "Country" => "US",
    //   "Plus4"=>"8943",
];


$BillingAddress = $ShipToAddress = $ShipFromAddress = $OrderPlacementAddress = $OrderApprovalAddress = $trump_tower;

$BillingAddress = $ShipToAddress = $ShipFromAddress = $OrderPlacementAddress = $OrderApprovalAddress = $my_address;

// $BillingAddress = $ShipToAddress = $ShipFromAddress = $OrderPlacementAddress = $OrderApprovalAddress = $microsoft;

// Web Request Header
$header["ClientNumber"] = env("CCH_CLIENT_ID");
$header["BusinessUnit"] = "My Biz Unit";
$header["ValidationKey"] = env("CCH_VALIDATION_KEY");

$header["DataYear"] = "2019";
$header["DataMonth"] = "1";
$header["TotalRevenue"] = 100.00;
$header["ReturnFileCode"] = "0";
$header["ClientTracking"] = "1234";
$header["ResponseType"] = "D";
// My additons
$header["CmplDataYear"] = "2019";
$header["CmplDataMonth"] = "1";


// End
// Input Record 1
$datarecord["LineNumber"] = "1";
$datarecord["InvoiceNumber"] = "112233";
$datarecord["CustomerNumber"] = "000000001";
$datarecord["LocationCode"] = "";
$datarecord["TransDate"] = "2019-01-17";
$now = Carbon::now();
$datarecord["TransDate"] = $now->toDateString();

$datarecord["Revenue"] = 100.00;
$datarecord["Units"] = 1;
$datarecord["TaxIncludedCode"] = "0";
$datarecord["TaxSitusRule"] = "22";
//$datarecord["TransTypeCode"] = "990101";    // Example value
$datarecord["TransTypeCode"] = "620101";    // My test  value clothing


$datarecord["SalesTypeCode"] = "D";
$datarecord["SalesTypeCode"] = "R";

$datarecord["RegulatoryCode"] = "01";
//$datarecord["RegulatoryCode"] = "99";

//$datarecord["TaxExemptionCodeList"] = array( "00");
$datarecord["UDF"] = "";
$datarecord["UDF2"] = "";
$datarecord["FreightOnBoard"] = "";
//$datarecord["ShipFromPOB"] = false;
$datarecord["ShipFromPOB"] = 1;

//$datarecord["MailOrder"] = false;
$datarecord["MailOrder"] = 1;

//$datarecord["CommonCarrier"] = false;
$datarecord["CommonCarrier"] = 1;

$datarecord["AuxRevenue"] = "0";
//$datarecord["AuxRevenue"] = "0m";

$datarecord["CostCenter"] = "";
$datarecord["GLAccount "] = "";
$datarecord["MaterialGroup "] = "";
$datarecord["CurrencyCode"] = "USD";
$datarecord["ExemptReasonCode"] = "";
//$datarecord["BillingAddress"] = $BillingAddress;
$datarecord["ShipToAddress"] = $ShipToAddress;
$datarecord["ShipFromAddress"] = $ShipFromAddress;
//$datarecord["OrderPlacementAddress"] = $OrderPlacementAddress;
//$datarecord["OrderApprovalAddress "] = $OrderApprovalAddress;

$url = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx";

$url="https://testapi.taxrating.net/Services/V07/SureTax.asmx/PostRequest";

// SureTax web request API URL
$suretax_url = "https://testapi.taxrating.net/Services/V07/SureTax.asmx/PostRequest";
$suretax_url = "https://testapi.taxrating.net/Services/V01/SureTax.asmx/PostRequest";
$suretax_url = "https://testapi.taxrating.net/Services/General/V01/SureTax.asmx/PostRequest";

echo "\n------------------\n";

echo "112\n";

$itemList[] = $datarecord;
$datarecord["LineNumber"] = "2";

$itemList[] = $datarecord;

print_r($itemList);
$x = SureTaxPostRequest::create($itemList,[
    'options' => ["ReturnFileCode"=>'T'],
]);

echo "Response\n";
print_r($x->__toArray(true));

echo "\n------------------\n";

echo "Finialize\n";
$fin = SureTaxFinalizePostRequest::create($x->TransId);

/*
$datarecord["LineNumber"] = "3";

$x->addItem($datarecord);
$x->refresh();
print_r($x->toArray());
*/


